<?php

namespace Duna\Extension\Manager;

interface IInstallable
{

	public function install();

	public function uninstall();
}

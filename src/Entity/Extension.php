<?php

namespace Duna\Extension\Manager\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="extension")
 * @ORM\Entity
 */
class Extension
{

	use \Kdyby\Doctrine\Entities\MagicAccessors;

	/**
	 * @var integer
	 *
	 * @ORM\Column(name="system_extension_id", type="integer", nullable=false)
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 */
	private $id;

	/**
	 * @var string
	 * 
	 * @ORM\Column(type="string", length=128, nullable=false)
	 */
	protected $name;

	/**
	 * @var string
	 * 
	 * @ORM\Column(type="string", unique=TRUE, nullable=false)
	 */
	protected $hash;

	/**
	 * @var type string
	 * 
	 * @ORM\Column(type="text", nullable=true)
	 */
	protected $description = null;

	/**
	 * @var boolean
	 * 
	 * @ORM\Column(type="boolean")
	 */
	protected $template = false;

	public function getId()
	{
		return $this->id;
	}

}

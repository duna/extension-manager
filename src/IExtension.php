<?php

namespace Duna\Extension\Manager;

interface IExtension
{

	public function getExtensionInfo();
}

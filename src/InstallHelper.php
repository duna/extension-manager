<?php

namespace Duna\Extension\Manager;

use Doctrine\ORM\EntityManagerInterface;
use Duna\Core\Localization\Entity\Locale;
use Duna\Core\Navigation\Entity\Navigation;
use Duna\Core\Navigation\Entity\NavigationItem;
use Duna\Core\Navigation\NavigationFacade;
use Duna\Plugin\SecurityComponent\Entity\Component;
use Duna\Plugin\SecurityComponent\Entity\PermissionRole;
use Duna\Router\Entity\Url;
use Duna\Router\Router;
use Duna\Security\Authorizator;
use Duna\Security\Entity\Permission;
use Duna\Security\Entity\Resource;
use Duna\Security\Entity\Role;
use Kdyby\Doctrine\EntityManager;
use Nette\Caching\Cache;
use Nette\Caching\IStorage;

/**
 * @author  Lukáš Černý <LukasCerny@hotmail.com>
 * @package Duna\Core\Extensions
 */
class InstallHelper
{

    /** @var EntityManager */
    private $em;

    private $cacheAuth;

    /** @var Cache */
    private $cacheRoute;

    /** @var Cache */
    private $cacheResource;

    /** @var NavigationFacade */
    private $facade;

    public function __construct(EntityManager $em, IStorage $cache, NavigationFacade $facade)
    {
        $this->em = $em;
        $this->cacheResource = new Cache($cache, Router::CACHE_NAMESPACE);
        $this->cacheRoute = new Cache($cache, Router::CACHE_NAMESPACE);
        $this->cacheAuth = new Cache($cache, Authorizator::CACHE_NAMESPACE);
        $this->facade = $facade;
    }

    public static function secureComponent(EntityManagerInterface $manager, $components, $reverse = false)
    {
        if ($reverse === false) {
            foreach ($components as $componet) {
                $entity = $manager->getRepository(Component::class)->findOneBy([
                    'resource' => (substr($componet['resource'], 0, 1) === '_' ? '' : '_') . $componet['resource'],
                ]);
                if ($entity === null)
                    $entity = new Component();
                $entity->name = $componet['name'];
                $entity->resource = (substr($componet['resource'], 0, 1) === '_' ? '' : '_') . $componet['resource'];
                $manager->persist($entity);
            }
            $manager->flush();
        } else {
            foreach ($components as $componet) {
                $entity = $manager->getRepository(Component::class)->findOneBy([
                    'resource' => (substr($componet['resource'], 0, 1) === '_' ? '' : '_') . $componet['resource'],
                ]);
                if ($entity !== null)
                    $manager->remove($entity);
            }
            $manager->flush();
        }
    }

    public static function secureComponentPermission(EntityManagerInterface $manager, $componentResource, $role, $create = false, $read = false, $update = false, $delete = false, $reverse = false)
    {
        if ($reverse === false) {
            $role = $manager->getRepository(Role::class)->findOneBy([
                'name' => $role,
            ]);
            $component = $manager->getRepository(Component::class)->findOneBy([
                'resource' => (substr($componentResource, 0, 1) === '_' ? '' : '_') . $componentResource,
            ]);
            $permission = new \Duna\Plugin\SecurityComponent\Entity\Permission();
            $permission->component = $component;
            $permission->role = $role;
            $permission
                ->setCreate($create)
                ->setRead($read)
                ->setUpdate($update)
                ->setDelete($delete);
            $manager->persist($permission);
            $manager->flush($permission);
        } else {
        }
    }

    public static function loadNavigationItems(EntityManagerInterface $manager, Navigation $nav, $items, $parentId = null, $reverse = false)
    {
        $facade = new NavigationFacade($manager);
        if ($reverse === false) {
            foreach ($items as $item) {
                $url = null;
                if (array_key_exists('link', $item)) {
                    $url = self::createUrl($item['link'], $item['presenter'], $item['action'], $manager->getRepository(Locale::class)->findOneBy(['code' => (isset($item['locale']) ? $item['locale'] : 'cs')]));
                    $manager->persist($url);
                }

                $navItem = new NavigationItem();
                $navItem->name = $item['name'];
                $navItem->url = $url;
                if (array_key_exists('icon', $item))
                    $navItem->icon = $item['icon'];

                $facade->createItem($navItem, $nav, $parentId);

                if (array_key_exists('items', $item))
                    self::loadNavigationItems($manager, $nav, $item['items'], $navItem->getId());
            }
        } else {
            foreach ($items as $item) {
                $navItem = $manager->getRepository(NavigationItem::class)->findOneBy(['name' => $item['name']]);
                if ($navItem) {
                    $manager->remove($navItem);
                    $manager->flush($navItem);
                }
                if (array_key_exists('link', $item)) {
                    $url = $manager->getRepository(Url::class)->findOneBy([
                        'presenter' => $item['presenter'],
                        'action'    => $item['action'],
                    ]);
                    if ($url) {
                        $manager->remove($url);
                        $manager->flush($url);
                    } else \Tracy\Debugger::log("Missing URL . (link=" . $item['presenter'] . ':' . $item['action']);
                }

                if (array_key_exists('items', $item))
                    self::loadNavigationItems($manager, $nav, $item['items'], $parentId, $reverse);
            }
        }
    }

    public static function createUrl($link, $presenter, $action, Locale $locale)
    {
        $entity = new Url;
        $entity->link = $link;
        $entity->presenter = $presenter;
        $entity->action = $action;
        $entity->locale = $locale;
        return $entity;
    }

    public static function urls(EntityManagerInterface $manager, array $urls, $reverse = false)
    {
        if ($reverse === false) {
            // Install
            foreach ($urls as $urlData) {
                $url = self::createUrl($urlData['link'], $urlData['presenter'], $urlData['action'], $manager->getRepository(Locale::class)->findOneBy(['code' => (isset($urlData['locale']) ? $urlData['locale'] : 'cs')]));
                $manager->persist($url);
                $manager->flush($url);
                \Tracy\Debugger::log('Installing URL ' . $url->getDestination());
            }
        } else {
            // Uninstall
            foreach ($urls as $urlData) {
                $url = $manager->getRepository(Url::class)->findOneBy([
                    'presenter' => $urlData['presenter'],
                    'action'    => $urlData['action'],
                ]);
                if ($url) {
                    \Tracy\Debugger::log('Uninstall URL ' . $url->getDestination());
                    $manager->remove($url);
                }
            }
        }
    }

    public static function invalidateUrlCache(IStorage $storage, $id)
    {
        $cache = new Cache($storage, Router::CACHE_NAMESPACE);
        $cache->cacheRoute->clean([Cache::TAGS => ['route/' . $id]]);
    }

    public function resource($resource, $name, $reverse = false)
    {
        $this->cacheAuth->clean([
            Cache::TAGS => [Authorizator::CACHE_NAMESPACE . '/resources'],
        ]);
        if ($reverse === false) {
            // Install
            $entity = new Resource;
            $entity->setResource($resource);
            $entity->setName($name);
            $this->em->safePersist($entity);
            \Tracy\Debugger::log('Installing resource ' . $resource);
            $this->cacheRoute->clean([Cache::TAGS => [Authorizator::CACHE_NAMESPACE . '/resources']]);
        } else {
            // Uninstall
            $entity = $this->em->getRepository(Resource::class)->findOneBy(['resource' => $resource]);
            if ($entity) {
                \Tracy\Debugger::log('Uninstall resource ' . $resource);
                $this->em->remove($entity);
                $this->cacheRoute->clean([Cache::TAGS => [Authorizator::CACHE_NAMESPACE . '/resources']]);
            }
        }
    }

    public function privilege($resource, $role, $allow = false, $read = false, $write = false, $create = false, $remove = false, $reverse = false)
    {
        if (!$reverse) {
            // Install
            $entity = new Permission;
            $entity->setAllow($allow);
            $entity->setCreate($create);
            $entity->setRead($read);
            $entity->setRemove($remove);
            $entity->setWrite($write);
            $entity->role = $this->em->getRepository(Role::class)->findOneBy(['name' => $role]);
            $entity->resource = $this->em->getRepository(Resource::class)->findOneBy(['resource' => $resource]);
            $this->em->safePersist($entity);
            \Tracy\Debugger::log('Installing privilege ' . $role . '->' . $reverse);
        } else {
            $entity = $this->em->getRepository(Permission::class)->findOneBy([
                'resource' => $this->em->getRepository(Resource::class)->findOneBy(['resource' => $resource]),
                'role'     => $this->em->getRepository(Role::class)->findOneBy(['name' => $role]),
            ]);
            if (!$entity) {
                \Tracy\Debugger::log('Uninstall privileges ' . $role . '->' . $resource);
                $this->em->remove($entity);
            }


        }
    }


}

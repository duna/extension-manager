<?php

namespace Duna\Extension\Manager\Cli;

use Kdyby\Doctrine\Console\OrmDelegateCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InstallCommand extends OrmDelegateCommand
{
    /** @var \Kdyby\Doctrine\Tools\CacheCleaner @inject */
    public $cacheCleaner;

    protected function initialize(InputInterface $input, OutputInterface $output)
    {
        parent::initialize($input, $output);
        $this->cacheCleaner->invalidate();
    }

    /**
     * @return \Symfony\Component\Console\Command\Command
     */
    protected function createCommand()
    {
        return new Commands\InstallCommand();
    }

}
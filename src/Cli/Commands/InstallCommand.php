<?php

namespace Duna\Extension\Manager\Cli\Commands;

use Doctrine\ORM\Tools\Console\Command\SchemaTool\AbstractCommand;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\Tools\ToolsException;
use Duna\Extension\Manager\DI\Extension;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class InstallCommand extends AbstractCommand
{

    protected function configure()
    {
        $this->setName('extensions:manager:install')
            ->setDescription('Create database schema for insalled extensions.');
    }

    protected function executeSchemaCommand(InputInterface $input, OutputInterface $output, SchemaTool $schemaTool, array $metadatas)
    {
        $emHelper = $this->getHelper('em');

        /* @var $em \Doctrine\ORM\EntityManager */
        $em = $emHelper->getEntityManager();

        $tables = [];
        $excluded = [];
        foreach ($metadatas as $metadata) {
            /** @var $metadata \Kdyby\Doctrine\Mapping\ClassMetadata */
            if (!in_array($metadata->namespace, array_keys(Extension::getEntityMappings())))
                $excluded[] = $metadata->table['name'];
            else
                $tables[] = $metadata;
        }
        try {
            $output->writeln('ATTENTION: This operation should not be executed in a production environment.' . PHP_EOL);
            $output->writeln('Creating database schema...');
            $schemaTool->createSchema($tables);
            $output->writeln('Database schema created successfully!');
        } catch (ToolsException $e) {
            $output->writeln('Database schema creating FAIL!');
        }
    }

}
<?php

namespace Duna\Extension\Manager;

use Duna\Extension\Manager\Entity\Extension;
use Duna\Security\Authorizator;
use Kdyby\Doctrine\EntityManager;
use Nette;
use Nette\DI\Container;

/**
 * @method onExtensionRegistration(Registrator $registrator)
 * @method onInstall()
 * @method onUninstal()
 */
class Registrator extends Nette\Object
{

    /** @var \Closure[] */
    public $onExtensionRegistration = [];

    /** @var EntityManager */
    private $em;

    /** @var Container */
    private $container;

    /**
     * @var array
     * @example [md5 => Extension $extension]
     */
    private $extensions = [];

    private $cache;

    public function __construct(EntityManager $em, Container $container, Nette\Caching\IStorage $storage)
    {
        $this->em = $em;
        $this->container = $container;
        $this->cache = new Nette\Caching\Cache($storage, Authorizator::CACHE_NAMESPACE);
    }

    public function registerExtensions(array $extensions)
    {
        $this->extensions = [];
        foreach ($extensions as $name) {
            $this->extensions[md5($name)] = (new $name)->getExtensionInfo();
        }
    }

    public function getExtensions()
    {
        $this->onExtensionRegistration($this);
        return array_map(function ($ext) {
            return $ext['ext'];
        }, $this->extensions);
    }

    public function installExtension($md5)
    {
        $this->cache->clean([
            Nette\Caching\Cache::TAGS => [Authorizator::CACHE_NAMESPACE]
        ]);
        $that = $this;
        $this->em->transactional(function () use ($that, $md5) {
            $extension = $that->getExtensionNames($md5);
            $extension['ext']->hash = $md5;
            $that->em->persist($extension['ext']);
            $that->em->flush($extension['ext']);
            if (isset($extension['install'])) {
                $installer = $that->container->getByType($extension['install']);
                $installer->install();
                $that->em->flush();
            }
            if (isset($extension['entity'])) {
                $metadata = array_values($extension['entity']);
                $path = array_pop($metadata);
                $classes = [];
                foreach ($that->getAllClassNames($path) as $class) {
                    $classes[] = $that->em->getClassMetadata($class);
                }
                $schema = new \Doctrine\ORM\Tools\SchemaTool($that->em);
                $schema->createSchema($classes);
            }
        });


    }

    public function getExtensionNames($key = null)
    {
        $this->onExtensionRegistration($this);
        if ($key === null)
            return $this->extensions;
        return $this->extensions[$key];
    }

    public function getAllClassNames($path)
    {
        $classes = [];
        $includedFiles = [];

        $iterator = new \RegexIterator(
            new \RecursiveIteratorIterator(
                new \RecursiveDirectoryIterator($path, \FilesystemIterator::SKIP_DOTS), \RecursiveIteratorIterator::LEAVES_ONLY
            ), '/^.+' . preg_quote('.php') . '$/i', \RecursiveRegexIterator::GET_MATCH
        );

        foreach ($iterator as $file) {
            $sourceFile = $file[0];

            if (!preg_match('(^phar:)i', $sourceFile)) {
                $sourceFile = realpath($sourceFile);
            }

            require_once $sourceFile;

            $includedFiles[] = $sourceFile;
        }


        $declared = get_declared_classes();

        foreach ($declared as $className) {
            $rc = new \ReflectionClass($className);
            $sourceFile = $rc->getFileName();
            if (in_array($sourceFile, $includedFiles)) {
                $classes[] = $className;
            }
        }

        return $classes;
    }

    public function uninstallExtension($md5)
    {
        $this->cache->clean([
            Nette\Caching\Cache::TAGS => [Authorizator::CACHE_NAMESPACE]
        ]);
        $that = $this;
        $this->em->transactional(function () use ($that, $md5) {
            $extension = $that->getExtensionNames($md5);
            $entity = $that->em->getRepository(Extension::class)->findOneBy(['hash' => $md5]);
            if ($entity) {
                $that->em->remove($entity);
                $that->em->flush($entity);
            }
            if (isset($extension['install'])) {
                $installer = $that->container->getByType($extension['install']);
                $installer->uninstall();
                $that->em->flush();
            }
            if (isset($extension['entity'])) {
                $metadata = array_values($extension['entity']);
                $path = array_pop($metadata);
                $classes = [];
                foreach ($this->getAllClassNames($path) as $class) {
                    $classes[] = $this->em->getClassMetadata($class);
                }
                $schema = new \Doctrine\ORM\Tools\SchemaTool($this->em);
                $schema->dropSchema($classes);
            }
        });

    }

}

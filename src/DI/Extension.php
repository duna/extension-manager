<?php

namespace Duna\Extension\Manager\DI;

use App;
use Composer\Autoload\ClassLoader;
use Duna\Extension\Manager\Cli\InstallCommand;
use Duna\Extension\Manager\IExtension;
use Duna\Extension\Manager\Registrator;
use Kdyby\Doctrine\DI\IEntityProvider;
use Nette\DI\Compiler;
use Nette\DI\CompilerExtension;
use Nette\Reflection\ClassType;
use Tracy\Debugger;

class Extension extends CompilerExtension implements IEntityProvider
{
    public static $extensionNames = [];
    private $commands = [
        InstallCommand::class
    ];

    public static function load(Compiler $compiler, ClassLoader $loader)
    {
        $iterator = 0;
        foreach ($loader->getClassMap() as $class => $file) {
            $reflection = ClassType::from($class);
            if ($reflection->implementsInterface(IExtension::class) && $reflection->isInstantiable()) {
                self::$extensionNames[] = $class;
                $compiler->addExtension('dynamic.extension.' . $iterator++, new $class);
            }
        }
    }

    public function beforeCompile()
    {
        $builder = $this->getContainerBuilder();

        $builder->getDefinition($builder->getByType(Registrator::class))
            ->addSetup(''
                . '?->onExtensionRegistration[] = function ($registrator) {' . "\n"
                . "\t" . '$registrator->registerExtensions(?);' . "\n"
                . '}', ['@self', self::$extensionNames]);
    }

    public function loadConfiguration()
    {
        \Duna\Core\DI\Extension::parseConfig($this, __DIR__ . '/config.neon');
        \Duna\Core\DI\Extension::loadCommands($this, $this->commands);
    }

    public function getEntityMappings()
    {
        return ["Duna\\Extension\\Manager\\Entity" => __DIR__ . '/../Entity'];
    }
}
